using System;
using System.Collections.Generic;
using System.Text;

namespace LV4ZD1i2adapter
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }



        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> list = dataset.GetData();
            double[][] data = new double[list.Count][];
            for (int i = 0; i < list.Count; i++)
            {
                data[i] = new double[list[i].Count];
                List<double> row = list[i];
                for (int j = 0; j < list[i].Count; j++)
                {
                    data[i][j] = row[j];
                }
            }
            return data;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
