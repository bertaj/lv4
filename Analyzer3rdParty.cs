using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LV4ZD1i2adapter
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }

        public double[] PerColumnAverage(double[][] data)
        {
            int columnCount = data[0].Length;
            double[] results = new double[columnCount];
            for(int i=0;i<columnCount; i++)
            {
                double columnSum = 0;
                for(int row=0; row<data.Length; row++)
                {
                    columnSum += data[row][i];
                }
                results[i] = columnSum / data.Length;
            }
            return results;
        }
    }
}
