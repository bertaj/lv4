using System;
using System.Collections.Generic;
using System.Text;

namespace LV4ZD1i2adapter
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
