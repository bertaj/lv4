using System;
using System.Collections.Generic;
using System.Text;

namespace LV4ZD1i2adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("C:\\Users\\No_name\\source\\repos\\LV4ZD1i2adapter\\LV4ZD1i2adapter\\csvfile.csv");
            Adapter adapter = new Adapter(new Analyzer3rdParty());
            double[] rowsAverages = adapter.CalculateAveragePerRow(dataset);
            double[] columnAverages = adapter.CalculateAveragePerColumn(dataset);
            IList<List<double>> List = dataset.GetData();
            foreach(List<double> list in List)
            {
                foreach (double n in list)
                {
                    Console.Write(n + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
            Console.WriteLine("Column Averages: ");
            foreach(double calcavg in columnAverages)
            {
                Console.Write(Math.Round(calcavg, 1) + "\t");
            }
            Console.WriteLine("\n");
            Console.WriteLine("\nRow Averages: ");
            foreach(double calcavg in rowsAverages)
            {
                Console.WriteLine(Math.Round(calcavg, 1));
            }
        }
    }
}
